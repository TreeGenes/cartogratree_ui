# CartograTree

## About

New iteration of cartogratree using mapbox instead of openlayers. 

## Built With
* [sparkles]()

## Planned features
* Phenotypes filter (WIP)
* Save queries and settings to your account
* New and exciting layers
* Move ordering of layers
* Region selector
* Export map
* Draw on map maybe
